¡Chat encriptado con módulo!
===

Modificación del engine del proyecto con sockets de ([Geekuillaume](http://geekuillau.me/)) para utilizar la encriptación de la clase de Matemática computacional.

Instalación:
===
Instalar [Node.js](https://nodejs.org/en/) ya sea desde [Chocolatey](https://chocolatey.org/) o a lo macho.
Luego de clonar el repositorio, instalar dependencias navegando al directorio donde se clonó y corriendo:

```
#!bash

npm install
```

Cómo correr:
===
Editar appPort en server.js (Configurado para heroku, cambiar a otro puerto en caso se quiera correr localmente, por ejemplo puerto 7777).

```
#!bash

node server.js
```

La lógica de encriptación se encuentra dentro de script.js, en el folder public. ¡Apretar F4 en el text box para escoger entre encriptado o no encriptado!